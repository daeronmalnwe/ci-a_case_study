#!/usr/bin/env bash

if [ -d .git ];
then
    echo 'Installing git hooks..'
    chmod a+x scripts/git-hooks/hooks/*
    cd .git/hooks
    ln -fFs ../../scripts/git-hooks/hooks/* .
fi;
